"""Installation script for flask-api-sourcedev application."""
from pathlib import Path
from setuptools import setup, find_packages

DESCRIPTION = ("")
APP_ROOT = Path(__file__).parent
README = (APP_ROOT / "README.md").read_text()
AUTHOR = "No Reply"
AUTHOR_EMAIL = "noreplydev.id@gmail.com"
PROJECT_URLS = {}
INSTALL_REQUIRES = [
    "Flask >= 2.0",
    "Flask-Bcrypt",
    "Flask-Cors",
    "Flask-Migrate",
    "flask-restx",
    "Flask-SQLAlchemy >= 3.0",
    "PyJWT==1.7.1",
    "python-dateutil",
    "python-dotenv",
    "requests",
    "urllib3",
    "werkzeug >= 2.0",
    "MarkupSafe>=2.0.1"
]
EXTRAS_REQUIRE = {
    "dev": [
        "black",
        "flake8 >= 4.0.0 and < 5.0.0",
        "pre-commit",
        "pydocstyle",
        "pytest",
        "pytest-black",
        "pytest-clarity",
        "pytest-dotenv",
        "pytest-flake8",
        "pytest-flask",
        "tox",
    ]
}

setup(
    name="flask-api-sourcedev",
    description=DESCRIPTION,
    long_description=README,
    long_description_content_type="text/markdown",
    version="0.1",
    author=AUTHOR,
    author_email=AUTHOR_EMAIL,
    maintainer=AUTHOR,
    maintainer_email=AUTHOR_EMAIL,
    url="",
    project_urls=PROJECT_URLS,
    packages=find_packages(where="src"),
    package_dir={"" : "src"},
    python_requires=">=3.6",
    install_requires=INSTALL_REQUIRES,
    extras_require=EXTRAS_REQUIRE,
)
