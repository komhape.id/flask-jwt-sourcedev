# flask-api-sourcedev

python3 -m venv venv\
. venv/bin/activate\

pip install --upgrade pip setuptools wheel\
pip install -e .\
flask db upgrade\
flask run\

open url for Swagger UI : http://localhost:5000/api/v1/ui

command for testing without run app: **_pytest_**