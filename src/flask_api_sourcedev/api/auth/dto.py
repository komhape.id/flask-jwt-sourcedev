"""Parsers and serializers for /auth API endpoints."""
from flask_restx import Model
from flask_restx.fields import String, Boolean
from flask_restx.inputs import email
from flask_restx.reqparse import RequestParser


auth_reqparser_register = RequestParser(bundle_errors=True)
auth_reqparser = RequestParser(bundle_errors=True)

#only for sign-up
auth_reqparser_register.add_argument(
    name="name", type=str, location="form", required=True, nullable=False
)
auth_reqparser_register.add_argument(
    name="age", type=str, location="form", required=True, nullable=False
)
auth_reqparser_register.add_argument(
    name="gender", type=str, location="form", required=True, nullable=False
)
auth_reqparser_register.add_argument(
    name="email", type=email(), location="form", required=True, nullable=False
)
auth_reqparser_register.add_argument(
    name="password", type=str, location="form", required=True, nullable=False
)


auth_reqparser.add_argument(
    name="email", type=email(), location="form", required=True, nullable=False
)
auth_reqparser.add_argument(
    name="password", type=str, location="form", required=True, nullable=False
)

user_model = Model(
    "User",
    {
        "email": String,
        "name": String,
        "age": String,
        "gender": String,
        "public_id": String,
        "admin": Boolean,
        "registered_on": String(attribute="registered_on_str"),
        "token_expires_in": String,
    },
)
